package groupteste.artteste;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetProperties {
	private InputStream inputStream;
	private String remoteIp;
	private String localIp;
	private String user;
	private String pwd;
	private String file;
	private String filePath;
	private String remotePath;
	private int port;

	
	public GetProperties() {
		super();
	}

	public void GetValues() throws IOException{
 
		try {
			Properties prop = new Properties();
			String propFileName = "sftp.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found!");
			}
 
			this.setRemoteIp(	prop.getProperty("sftp.remoteIp")						);
			this.setLocalIp(	prop.getProperty("sftp.localIp")						);
			this.setUser( 		prop.getProperty("sftp.user")							);
			this.setPwd(		prop.getProperty("sftp.pwd")							);
			this.setFile(		prop.getProperty("sftp.file")							);
			this.setRemotePath(	prop.getProperty("sftp.remotePath")						);
			this.setFilePath(	prop.getProperty("sftp.filePath")						);
			this.setPort(		Integer.parseInt( prop.getProperty("sftp.port") )		);
 
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}finally {
			inputStream.close();
		}
	}
	
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getRemoteIp() {
		return remoteIp;
	}

	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}	
	
	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}
		
}

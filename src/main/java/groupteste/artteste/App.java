package groupteste.artteste;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

/**
 * @author Nuno Marques
 *
 */
public class App {

    /**
     * @param args
     */
    public static void main(String[] args) {
        String SFTPHOST			= "";
        int SFTPPORT			= 0 ;
        String SFTPUSER			= "";
        String SFTPPASS 		= "";
        String SFTPWORKINGDIR	= "";
        String FILETOTRANSFER	= "";
        
    	try {
			GetProperties props = new GetProperties();
			props.GetValues();
			
	        SFTPHOST = props.getRemoteIp();
	        SFTPPORT = props.getPort();
	        SFTPUSER = props.getUser();
	        SFTPPASS = props.getPwd();
	        SFTPWORKINGDIR = props.getRemotePath();
	        FILETOTRANSFER = props.getFilePath() + "" +props.getFile();			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;

        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel("sftp");
            channel.connect();
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPWORKINGDIR);
            
            File f = new File(FILETOTRANSFER);
            channelSftp.put(new FileInputStream(f), f.getName());
            
            //close sessions here
            channelSftp.exit();
            session.disconnect();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
}


